# -*- coding: utf-8 -*-
"""

"""
import os
from flask import Flask, request, session, g, redirect, url_for, abort, \
     render_template, flash
from werkzeug import secure_filename     
from forms import UploadForm


# create our little application :)
app = Flask(__name__)

# Load default config and override config from an environment variable
app.config.update(dict(
    DEBUG=True,
    SECRET_KEY='sadasfsdhj2347sadb21912sdjghfdskg',
    UPLOAD_PATH="/tmp/"
))


@app.route('/', methods=['GET', 'POST'])
def index():
    g.form = UploadForm()
    return render_template('index.html')

@app.route('/upload/', methods=['GET', 'POST'])
def upload():
    file = request.files.get('image')
    if file:
        filename = secure_filename(file.filename)
        filepath = os.path.join(app.config.get("UPLOAD_PATH"), filename)
        file.save(filepath)
    return redirect(url_for('index'))


if __name__ == '__main__':
    app.run()
