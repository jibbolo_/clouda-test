from wtforms import Form, FileField, validators

class UploadForm(Form):
    image        = FileField(u'Image File')

    def validate_image(form, field):
        if field.data:
            field.data = re.sub(r'[^a-z0-9_.-]', '_', field.data)
